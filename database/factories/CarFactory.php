<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Car;
use Faker\Generator as Faker;
use App\User;
use App\Driver;

$factory->define(Car::class, function (Faker $faker)  use ($factory) {
    return [
        //
        'panel_number'=>$faker->randomNumber(8),
    'model'=>$faker->text(6),
        'color'=>$faker->colorName(),
        'driver_id'=>$factory->create(App\Driver::class)->id




    ];
});
