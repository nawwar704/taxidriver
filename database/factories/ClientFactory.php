<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Client;
use Faker\Generator as Faker;

$factory->define(Client::class, function (Faker $faker) use ($factory){
    return [
        //

        'address'=>$faker->address(),
        'is_driver'=>$faker->boolean(5),
        'points'=>$faker->numberBetween(0,1000000),
        'personal_photo'=>$faker->imageUrl(),
        'user_id'=>$factory->create(App\User::class)->id

    ];
});
