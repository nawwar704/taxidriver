<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Ride;
use Faker\Generator as Faker;

$factory->define(Ride::class, function (Faker $faker) use ($factory) {
    return [
        //
        'departure_location'=>$faker->address,
        'destination'=>$faker->address,
        'is_done'=>$faker->boolean(),
        'driver_id'=>$factory->create(App\Driver::class)->id,
        'user_id'=>$factory->create(App\User::class)->id,
        'car_id'=>$factory->create(App\Car::class)->id,
        'number_of_seats'=>random_int(0,4)
    ];
});
