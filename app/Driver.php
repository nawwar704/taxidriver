<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Car;
use App\Ride;

class Driver extends Model
{
    //
    protected $fillable = ['license', 'working days'];

    public function user()
    {
        return $this->morphOne('App\User','user');
    }
    public function cars(){
        return $this->hasMany('App\Car');
    }
    public function rides(){
        return $this->hasMany(Ride::class);
    }

}
