<?php

namespace App;

use App\Driver;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    //adding relationship between each car and its owner
    protected $fillable = ['driver_id', 'panel_number', 'model', 'color'];

    public function driver()
    {
        return $this->belongsTo(Driver::class);
    }

}
