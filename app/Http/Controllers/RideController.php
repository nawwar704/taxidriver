<?php

namespace App\Http\Controllers;

use App\Driver;
use App\Ride;
use App\User;
use App\Car;
use App\Mail\CarOrder;
use Illuminate\Contracts\Database;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use function GuzzleHttp\Promise\all;

class RideController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function findRide(User $user, Request $request)
    {
        $request->validate([
            'time' => 'required',
            'departureLocation' => 'required',
            'destination' => 'required',
            'seats'=>'required',

        ]);
        $departure_location = $request->departureLocation;
        $destination = $request->destination;
        $time = $request->time;
        $seats=$request->seats;
        $phone_number=$request->phone;

        $user_id = $user->id;
        /*$rides=DB::table(rides)
            ->select(all())
            ->where('destination','like',$destination)
            ->where('time_to_go','like',$time);*/
        $rides = Ride::where(['destination' => $destination, 'time_to_go' => $time,'number_of_seats'=>$seats])->get();

       // dd($details);
        dd($rides);
        return $rides->toJson() ;
    }
    public function shareRide(User $user, Request $request)
    {

        $request->validate([
            'time' => 'required',
            'departureLocation' => 'required',
            'destination' => 'required',


        ]);

        $departure_location = $request->departureLocation;
        $destination = $request->destination;
        $time = $request->time;
        $user_id = $user->id;

        $car_id = $request->car;
        $car = Car::find($car_id);



        $driver = Driver::find($user->user_id);

        if ($driver != null) {
            $ride = Ride::updateOrcreate([
                'destination' => $destination,
                'departure_location' => $departure_location,
                'time_to_go' => $time,
                'user_id'=>1,

                'driver_id' => $driver->id,
                'car_id' => $car->id,
                'number_of_seats' => 2
            ]);





            return redirect()->back()->with('alert', 'you just shared a ride ');
        }
        return redirect()->back()->with('alert', 'you should register as a driver first');
    }
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function bookRide(Request $request, User $user ,Ride $ride)
    {

        $departure_location = $ride->departure_location;
        $destination = $ride->destination;
        $time = $ride->time_to_go;
        $driver_name=$ride->driver->user->name;


        $phone_number=$user->phone_number;
        $driver_email_address=$ride->driver->user->email;

        $details=array('destination'=>$destination,'time_to_go'=>$time,'driver_name'=>$driver_name,
            'location'=>$departure_location,'phone_number'=>$phone_number);


        $mail=new CarOrder($details);
        Mail::to('nawwarcodavia@gmail.com')->send($mail);
        return redirect()->back()->with('alert','your driver will be here soon!');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function show(Ride $ride)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function edit(Ride $ride)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ride $ride)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Ride  $ride
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ride $ride)
    {
        //
    }
}
