<?php

namespace App\Http\Controllers;

use App\Ride;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)

    {

        $fullurl = $request->url();
        $token = $request->query('token');
        $content = 'redirect';



        //   header('Authentication', $token);
        // unset($request['token'] );
     // $ride = Ride::latest()->paginate(10);

        $rides = Ride::latest()->paginate(10);
//dd($rides[0]->driver->id);

        return view('home', compact('rides'));
    }


}
