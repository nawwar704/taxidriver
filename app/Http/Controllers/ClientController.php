<?php

namespace App\Http\Controllers;

use App\Client;
use App\Driver;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class ClientController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param Client $client
     * @return Response
     */
    public function show($id)
    {
        //
        // dd($id);
        $user = User::find($id);
        if ($user->user_type == 'driver') {
            $driver_id = $user->userable_id;
            $driver = Driver::find($driver_id);

            //  dd($cars);
            $cars = $driver->cars;
            // dd($cars[0]->model);

            return view('users.profile.profile', compact('user', 'driver', 'cars'));
        } else {
            return view('users.profile.profile', compact('user'));
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Client $client
     * @return Response
     */
    public function edit(Client $client)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param Client $client
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {


        $user = User::find($id);

        $validator = Validator::make($request->all(), [
            'name' => 'string|between:2,100',
            'email' => 'string|email|max:100',

            'address' => ['max:30'],


        ]);

        if($validator->fails()){
            return response()->json($validator->messages(), Response::HTTP_BAD_REQUEST);
        }

        if ($request->hasFile('photo')) {
            $photo = $request->photo;

            $name = $request->name;
            $extension = $photo->extension();
            $photoName = 'personal_photos' . $name . '.' . $extension;
            $path = $name . '/';
            Storage::disk('public')->put($path . $photoName, $photo);
            //  dd($request->email);



        }
        $user->update(
            $request->all()


        );
        return \response()->json([
            'message' => 'Client has been updated successfully'
        ]);

        //dd($user);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Client $client
     * @return Response
     */
    public function destroy(Client $client)
    {
        //
    }

    public function addpoints($id, Request $request)
    {
        $user = User::find($id);

        $points = $request->pointsCount;
        if ($points > 0) {


            $user->update(['points' => $points]);
        }
        // dd($client);
        return redirect('home');
    }
}
