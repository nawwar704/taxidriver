<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\AuthenticatesUsers;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\RegistersUsers;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Auth;

use App\Client;


use Illuminate\Http\Request;
use App\User;


class AuthController extends Controller
{
    use AuthenticatesUsers;

    /**
     * Create a new AuthController instance.
     *
     * @return void
     */


    /**
     * Get a JWT via given credentials.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required|string|min:6',
        ]);
        //dd($request);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 422);
        }

        if (!$token = auth('api')->attempt($validator->validated())) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }
       // return response(200)->header($this->createNewToken($token));

        return $this->createNewToken($token);
    }

    /**
     * Register a User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(Request $request)
    {
      //  dd($request);
        $validator = Validator::make($request->all(), [
            'name' => 'required|string|between:2,100',
            'email' => 'required|string|email|max:100|unique:users',
            'password' => 'required|string|confirmed|min:6',
            'address' => ['required', 'max:30'],
            'photo' => ['required', 'mimes:jpeg,png'],

            'phone'=>['required', 'max:10']
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors()->toJson(), 400);
        }
        $photo = $request->photo;
        $name = $request->name;
        $extension = $photo->extension();
        $photoName = $name . '.' . $extension;
        $path = 'public/personalphotos/' . $name . '/';
        $photo->storeAs($path, $photoName);
        //$client = Client::create(['address' => $request->address, 'points' => 0, 'personal_photo' => $photoName]);

        $user =  User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone_number'=>$request->phone,
            'user_id'=>1,

            'user_type' => 'App\User',
            'address' => $request->address,
            'points' => 0,
            'personal_photo' => $photoName,

            'password' => Hash::make($request->password),
        ]);
      //dd($user);
         return redirect('login');
       /* return response()->json([
            'message' => 'User successfully registered',
            'user' => $user,

        ], 201);*/
    }


    /**
     * Log the user out (Invalidate the token).
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function logout()
    {
        auth('api')->logout();

        return response()->json(['message' => 'User successfully signed out']);
    }

    /**
     * Refresh a token.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function refresh()
    {
        return $this->createNewToken(auth()->refresh());
    }

    /**
     * Get the authenticated User.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function userProfile()
    {
        return response()->json(auth()->user());
    }

    /**
     * Get the token array structure.
     *
     * @param  string $token
     *
     * @return \Illuminate\Http\JsonResponse
     */
    protected function createNewToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            // 'expires_in' => auth()->factory()->getTTL() * 60,
            'user' => auth('api')->user()
        ]);
    }
}
