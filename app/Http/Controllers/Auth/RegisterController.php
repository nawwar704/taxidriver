<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use http\Env\Request;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'address' => ['required', 'max:30'],
            'photo' => ['required', 'mimes:jpeg,png']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        // dd($data);
        $photo = $data['photo'];
        $name = $data['name'];
        $extension = $photo->extension();
        $photoName = $name . '.' . $extension;
        $path = 'public/personalphotos/' . $name . '/';
        $photo->storeAs($path, $photoName);


        return User::UpdateOrCreate([
            'name' => $data['name'],
            'email' => $data['email'],
            'user_id'=>1,
            'phone_number'=>$data['phone'],

            'user_type' => 'App\User',
            'address' => $data['address'],
            'points' => 0,
            'personal_photo' => $photoName,

            'password' => Hash::make($data['password']),
        ]);
        //dd($user);
        //  return $user;
        //return redirect(route('home'));


    }
}
