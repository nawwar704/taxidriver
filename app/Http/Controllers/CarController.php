<?php

namespace App\Http\Controllers;

use App\Car;
use App\Driver;
use App\User;
use Illuminate\Http\Request;

class CarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        //
        return 'hi';
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,User $user)
    {
        //


        $request->validate([
            'model' => 'required|string|between:2,100',
            'panelNumber'=>'required|between:2,15',

           'color'=> 'required|string|between:2,10'
        ]);
      $driver=Driver::find($user->user_id);
      //dd($driver);
          if($driver!=null) {



              $model = $request->model;
              $panel_number = $request->panelNumber;
              $color = $request->color;
              $car = Car::create([
                  'model' => $model,
                  'panel_number' => $panel_number,
                  'driver_id' => $driver->id,
                  'color' => $color

              ]);
           //   dd($car->driver);


            //  dd($car);
              return redirect('home');
          }
           return redirect()->back()->with('alert','you should register as a driver first');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function show(Car $car)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function edit(Car $car)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $users)
    {
        //

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Car  $car
     * @return \Illuminate\Http\Response
     */
    public function destroy(Car $car)
    {
        //
        dd($car);
    }
}
