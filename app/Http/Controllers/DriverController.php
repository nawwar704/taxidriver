<?php

namespace App\Http\Controllers;

use App\Driver;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DriverController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index(Request $request)
    {
        //
        if ($request->hasFile('img')) {
            $file = $request->file('img');
            $extension = $request->file('img')->extension();

            $name = Auth::user()->name;
            $img_name = $name . '.' . $extension;
            $path = 'public/personalphoto/' . $name;

            $file->storeAs($path, $img_name);
            dd($img_name);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('users.upload');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, User $user)
    {
        //
        //dd($user);


        if ($request->hasFile('photo')) {
            if ($request->file('photo')->isValid()) {
                $request->validate(['photo' => 'mimes:jpeg,png']);

                $photo = $request->photo;
                $name = $user->name;
                $extension = $photo->extension();
                $photoName = $name . '.' . $extension;
                $path = 'public/license/' . $name . '/';
                $photo->storeAs($path, $photoName);

                $driver = Driver::UpdateOrCreate(['license' => $photoName]);
              //  dd($driver->id);
                $driver_id = $driver->id;


                $user->update(['user_type' => 'App\Driver']);
                $user->update(['user_id' => $driver_id]);


            }
        }

        return redirect('home')->with('alert', 'you are now a driver in our company ');
    }

    /**
     * Display the specified resource.
     *
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function edit(Driver $driver)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Driver $driver)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param \App\Driver $driver
     * @return \Illuminate\Http\Response
     */
    public function destroy(Driver $driver)
    {
        //
    }
}
