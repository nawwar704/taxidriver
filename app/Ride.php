<?php

namespace App;
use App\User;
use App\Driver;
use App\Car;
use Illuminate\Database\Eloquent\Model;

class Ride extends Model
{
    //
    protected $fillable=['departure_location','destination','time_to_go','car_id','user_id','driver_id','number_of_seats'];
    public function driver(){
        return $this->belongsTo(Driver::class,'driver_id');
    }
    public function rider(){
        return $this->belongsTo(User::class,'user_id');
    }
    public function car(){
        return $this->hasOne(Car::class, 'id', 'car_id');
    }

}
