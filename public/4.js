(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[4],{

/***/ "./resources/js/ongoing-ride/hide.js":
/*!*******************************************!*\
  !*** ./resources/js/ongoing-ride/hide.js ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../bootstrap */ "./resources/js/bootstrap.js");

$('.ongoing-ride-close').click(hide);

function hide() {
  $('.fixed-bottom').css('visibility', 'hidden');
}

/***/ })

}]);