(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[5],{

/***/ "./resources/js/points/addPoints.js":
/*!******************************************!*\
  !*** ./resources/js/points/addPoints.js ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../bootstrap */ "./resources/js/bootstrap.js");

$('#pointsCount').keyup(changeValue);

function changeValue(event) {
  var coinsIcon = '<i class="fa fa-coins"></i>';
  $('#pointsPrice').html($(this).val() + '$ ' + coinsIcon);
}

/***/ })

}]);