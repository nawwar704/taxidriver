(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[7],{

/***/ "./resources/js/sidebar/toggle.js":
/*!****************************************!*\
  !*** ./resources/js/sidebar/toggle.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var $sidebarToggler = $('.sidebar-toggle');
var $sidebar = $('.sidebar');
$sidebarToggler.click(toggleSidebar);
var close = true;

function toggleSidebar() {
  if (close) {
    closeSidebar();
  } else {
    openSidebar();
  }

  close = !close;
}

function closeSidebar() {
  $sidebar.animate({
    left: '-240px'
  });
  $sidebarToggler.animate({
    borderSpacing: 180
  }, {
    step: function step(now, fx) {
      $(this).css('-webkit-transform', 'translateY(-50%) rotate(' + now + 'deg)');
      $(this).css('-moz-transform', 'translateY(-50%) rotate(' + now + 'deg)');
      $(this).css('transform', 'translateY(-50%) rotate(' + now + 'deg)');
    },
    duration: 'slow'
  }, 'linear');
}

function openSidebar() {
  $sidebar.animate({
    left: '0'
  });
  $sidebarToggler.animate({
    borderSpacing: 0
  }, {
    step: function step(now, fx) {
      $(this).css('-webkit-transform', 'translateY(-50%) rotate(' + now + 'deg)');
      $(this).css('-moz-transform', 'translateY(-50%) rotate(' + now + 'deg)');
      $(this).css('transform', 'translateY(-50%) rotate(' + now + 'deg)');
    },
    duration: 'slow'
  }, 'linear');
}

/***/ })

}]);