(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[6],{

/***/ "./resources/js/scrolling/jumbotron-scroll.js":
/*!****************************************************!*\
  !*** ./resources/js/scrolling/jumbotron-scroll.js ***!
  \****************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../bootstrap */ "./resources/js/bootstrap.js");

var jumbotron = $('.jumbotron');
var $window = $(window);
var navbar = $('.navbar');
var jumbotronButtons = $('.jumbotron-divider');
var hr = $(jumbotron).find('hr');
var pos = jumbotron.height() - jumbotronButtons.height() + hr.outerHeight(true) / 2;
var jumbotronContainer = $('.fixed-jumbotron-height');
jumbotronContainer.css('height', jumbotron.outerHeight()); //jumbotronContainer.css('padding-top', navbar.outerHeight())

var jumbotronButtonsLg = $('.btn-lg');
var fontSize = jumbotronButtonsLg.css('font-Size');
jumbotron.ready(function () {
  if (jumbotron.length <= 0) return;
  fontSize = fontSize.slice(0, -2);
  $window.resize(function () {
    pos = jumbotron.height() - jumbotronButtons.height() + hr.outerHeight(true) / 2;
    jumbotronContainer = $('.fixed-jumbotron-height');
    jumbotronContainer.css('height', jumbotron.outerHeight()); //jumbotronContainer.css('padding-top', navbar.outerHeight())

    fontSize = jumbotronButtonsLg.css('font-Size');
    fontSize = fontSize.slice(0, -2);
    scrollingCheck();
  });
  $window.on('load', function () {
    scrollingCheck();
  });
  $window.scroll(scrollingCheck);
});

function scrollingCheck() {
  var scrollTop = $window.scrollTop();

  if (scrollTop >= pos) {
    jumbotron.css({
      position: 'fixed',
      top: -pos
    });
    jumbotron.removeClass('more-transparent-animation');
    setTimeout(function () {
      jumbotron.addClass("darker-animation");
    }, 50);
    changeTextSize(scrollTop);
  } else {
    jumbotron.css({
      position: 'static',
      top: 0
    });
    jumbotron.remove('darker-animation');
    setTimeout(function () {
      jumbotron.addClass('more-transparent-animation');
    }, 50);
    jumbotronButtonsLg.css('font-size', fontSize + "px");
  }
}

function changeTextSize(newPos) {
  var posDifference = newPos - pos;
  posDifference /= 10;
  var newFont = fontSize - posDifference; //console.log(fontSize + ' + ' + posDifference + ' = ' + newFont)

  if (newFont >= 12) {
    jumbotronButtonsLg.css('font-size', newFont + 'px');
  } else {
    jumbotronButtonsLg.css('font-size', '12px');
  }
}

/***/ })

}]);