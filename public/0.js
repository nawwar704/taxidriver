(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[0],{

/***/ "./resources/js/images/image-viewer.js":
/*!*********************************************!*\
  !*** ./resources/js/images/image-viewer.js ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var imageViewer = $('.image-viewer');
var imageViewerImage = $('.image-viewer-image img');
$('.image').click(showImage);
imageViewer.click(hideImage);

function showImage(event) {
  var imageUrl = $(event.target).attr('src');
  imageViewer.css('visibility', 'visible');
  imageViewer.animate({
    opacity: 1
  }, 1000);
  imageViewerImage.attr('src', imageUrl);
}

function hideImage() {
  imageViewer.animate({
    opacity: 0
  }, 1000, function () {
    imageViewer.css('visibility', 'hidden');
  });
}

/***/ })

}]);