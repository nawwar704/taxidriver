(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[8],{

/***/ "./resources/js/shared/configurations.js":
/*!***********************************************!*\
  !*** ./resources/js/shared/configurations.js ***!
  \***********************************************/
/*! exports provided: config, messages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "config", function() { return config; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "messages", function() { return messages; });
var config = {
  url: '//127.0.0.1:8000/',
  auth: {
    csrfToken: $('meta[name="csrf-token"]').attr('content')
  },
  mainTemplate: $('main')
};
var messages = {
  error: {
    badge: '<span class="badge badge-danger error-badge">@message</span>'
  },
  success: {
    alert: '<div class="alert alert-success alert-dismissible fade show" role="alert">\n' + '        @message\n' + '        <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' + '            <span aria-hidden="true">&times;</span>\n' + '        </button>\n' + '    </div>'
  }
};


/***/ }),

/***/ "./resources/js/user/client/update.js":
/*!********************************************!*\
  !*** ./resources/js/user/client/update.js ***!
  \********************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _shared_configurations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../shared/configurations */ "./resources/js/shared/configurations.js");

var userNameField = $('#userName');
var emailField = $('#email');
var addressField = $('#address');
var imageField = $('#photo');
$('#editClientData').click(updateClientData);

function updateClientData() {
  $('.error-badge').remove();
  var userName = userNameField.val();
  var email = emailField.val();
  var address = addressField.val();
  var image = imageField.val();
  sendUpdateClientRequest(userName, email, address, image);
}

function sendUpdateClientRequest(userName, email, address, image) {
  var userId = localStorage.getItem('id');
  var requestUrl = '../update/1'; //;

  $.ajax({
    url: requestUrl,
    type: 'PUT',
    headers: {
      'X-CSRF-TOKEN': _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["config"].auth.csrfToken
    },
    data: {
      'name': userName,
      'email': email,
      'address': address,
      'photo': image
    },
    success: updateDone,
    error: cantUpdate
  });
}

function updateDone(response) {
  console.log(response);
  _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["config"].mainTemplate.prepend(_shared_configurations__WEBPACK_IMPORTED_MODULE_0__["messages"].success.alert.replace(/@message/, response.message));
  $('.modal').modal('hide');
}

function cantUpdate(response) {
  console.log(response);
  response = response.responseJSON;
  console.log(response.email);
  var errorMessage = "";

  if (response['email'] != null) {
    for (var i = 0; i < response.email.length; i++) {
      errorMessage += response.email[i] + '\n';
    }

    var emailMessage = _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["messages"].error.badge.replace(/@message/, errorMessage);
    $(emailMessage).insertAfter(emailField);
  }

  errorMessage = '';

  if (response.name != null) {
    for (var _i = 0; _i < response.name.length; _i++) {
      errorMessage += response.name[_i] + '\n';
    }

    var _emailMessage = _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["messages"].error.badge.replace(/@message/, errorMessage);

    $(_emailMessage).insertAfter(userNameField);
  }

  errorMessage = '';

  if (response.address != null) {
    for (var _i2 = 0; _i2 < response.address.length; _i2++) {
      errorMessage += response.address[_i2] + '\n';
    }

    var _emailMessage2 = _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["messages"].error.badge.replace(/@message/, errorMessage);

    $(_emailMessage2).insertAfter(addressField);
  }

  errorMessage = '';

  if (response.photo != null) {
    for (var _i3 = 0; _i3 < response.photo.length; _i3++) {
      errorMessage += response.photo[_i3] + '\n';
    }

    var _emailMessage3 = _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["messages"].error.badge.replace(/@message/, errorMessage);

    $(_emailMessage3).insertAfter(imageField);
  }

  errorMessage = '';

  if (response.password != null) {
    for (var _i4 = 0; _i4 < response.password.length; _i4++) {
      errorMessage += response.password[_i4] + '\n';
    }

    var _emailMessage4 = _shared_configurations__WEBPACK_IMPORTED_MODULE_0__["messages"].error.badge.replace(/@message/, errorMessage);

    $(_emailMessage4).insertAfter(imageField);
  }
}

/***/ })

}]);