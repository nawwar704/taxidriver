(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[1],{

/***/ "./resources/js/images/imageLoader.js":
/*!********************************************!*\
  !*** ./resources/js/images/imageLoader.js ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__(/*! ../bootstrap */ "./resources/js/bootstrap.js");

$('.imageFileLoader').change(function () {
  readURL(this);
});

function readURL(input) {
  if (input.files && input.files[0]) {
    var reader = new FileReader();

    reader.onload = function (e) {
      $('.imagePlace').attr('src', e.target.result);
    };

    reader.readAsDataURL(input.files[0]);
  }
}

/***/ })

}]);