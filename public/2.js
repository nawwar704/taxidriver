(window["webpackJsonp"] = window["webpackJsonp"] || []).push([[2],{

/***/ "./resources/js/layouts/navbar.js":
/*!****************************************!*\
  !*** ./resources/js/layouts/navbar.js ***!
  \****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

$('body').ready(function () {
  var body = $('body');
  var navbar = $('.navbar');
  var $window = $(window);
  body.css('paddingTop', navbar.outerHeight() + 'px');
  $window.resize(function () {
    body.css('paddingTop', navbar.outerHeight() + 'px');
  });
});

/***/ })

}]);