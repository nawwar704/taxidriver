require('../bootstrap')
let jumbotron = $('.jumbotron');
let $window = $(window);
let navbar = $('.navbar')
let jumbotronButtons = $('.jumbotron-divider');
let hr = $(jumbotron).find('hr');
let pos = jumbotron.height() - jumbotronButtons.height() + hr.outerHeight(true) / 2;
let jumbotronContainer = $('.fixed-jumbotron-height');
jumbotronContainer.css('height', jumbotron.outerHeight());
//jumbotronContainer.css('padding-top', navbar.outerHeight())
let jumbotronButtonsLg = $('.btn-lg')
let fontSize = jumbotronButtonsLg.css('font-Size')

jumbotron.ready(function () {
    if (jumbotron.length <= 0)
        return

    fontSize = fontSize.slice(0, -2);

    $window.resize(function () {
        pos = jumbotron.height() - jumbotronButtons.height() + hr.outerHeight(true) / 2;
        jumbotronContainer = $('.fixed-jumbotron-height')
        jumbotronContainer.css('height', jumbotron.outerHeight())
        //jumbotronContainer.css('padding-top', navbar.outerHeight())
        fontSize = jumbotronButtonsLg.css('font-Size')
        fontSize = fontSize.slice(0, -2)
        scrollingCheck();
    })

    $window.on('load', function () {
        scrollingCheck();
    });

    $window.scroll(scrollingCheck);

})

function scrollingCheck() {

    let scrollTop = $window.scrollTop();
    if (scrollTop >= pos)
    {
        jumbotron.css({
            position: 'fixed',
            top: -pos,
        })

        jumbotron.removeClass('more-transparent-animation')

        setTimeout(function () {
            jumbotron.addClass("darker-animation");
        }, 50)

        changeTextSize(scrollTop)
    }
    else {
        jumbotron.css({
            position: 'static',
            top: 0,
        });

        jumbotron.remove('darker-animation')

        setTimeout(function () {
            jumbotron.addClass('more-transparent-animation')
        }, 50)

        jumbotronButtonsLg.css('font-size', fontSize + `px`)
    }
}

function changeTextSize(newPos) {
    let posDifference = newPos - pos;
    posDifference /= 10;
    let newFont = fontSize - posDifference;
    //console.log(fontSize + ' + ' + posDifference + ' = ' + newFont)
    if (newFont >= 12)
    {
        jumbotronButtonsLg.css('font-size', newFont + 'px')
    }
    else {
        jumbotronButtonsLg.css('font-size', '12px')
    }
}
