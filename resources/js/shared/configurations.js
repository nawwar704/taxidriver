
let config = {
    url:  '//127.0.0.1:8000/',
    auth: {
        csrfToken: $('meta[name="csrf-token"]').attr('content'),
    },
    mainTemplate: $('main')
}

let messages = {
    error: {
        badge: '<span class="badge badge-danger error-badge">@message</span>'
    },
    success: {
        alert: '<div class="alert alert-success alert-dismissible fade show" role="alert">\n' +
            '        @message\n' +
            '        <button type="button" class="close" data-dismiss="alert" aria-label="Close">\n' +
            '            <span aria-hidden="true">&times;</span>\n' +
            '        </button>\n' +
            '    </div>',
    }
}

export {config, messages}
