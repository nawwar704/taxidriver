const imageViewer = $('.image-viewer');
const imageViewerImage = $('.image-viewer-image img');

$('.image').click(showImage);
imageViewer.click(hideImage);

function showImage(event) {
    const imageUrl = $(event.target).attr('src');
    imageViewer.css('visibility', 'visible');
    imageViewer.animate({
        opacity: 1
    }, 1000)

    imageViewerImage.attr('src', imageUrl);
}

function hideImage() {
    imageViewer.animate({
        opacity: 0
    }, 1000, function () {
        imageViewer.css('visibility', 'hidden');
    })
}
