require('./bootstrap');
import ('../js/images/imageLoader')
import ('./ongoing-ride/hide')
import('../js/images/image-viewer')
import('../js/points/addPoints')

// Scrolling
import('../js/scrolling/jumbotron-scroll')

// Layouts
import('../js/layouts/navbar')


// Auth
import '../js/auth/login'

// Sidebar
import('../js/sidebar/toggle')

// Client Processing
import('../js/user/client/update')
