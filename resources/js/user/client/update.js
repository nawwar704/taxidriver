import {config, messages} from '../../shared/configurations'
let userNameField = $('#userName');
let emailField = $('#email');
let addressField = $('#address');
let imageField = $('#photo');

$('#editClientData').click(updateClientData);

function updateClientData() {
    $('.error-badge').remove();
    let userName = userNameField.val();
    let email = emailField.val();
    let address = addressField.val();
    let image = imageField.val();

    sendUpdateClientRequest(userName, email, address, image);
}

function sendUpdateClientRequest(userName, email, address, image) {
    let userId = localStorage.getItem('id');
    let requestUrl = '../update/1' //;

    $.ajax({
        url: requestUrl,
        type: 'PUT',
        headers: {
            'X-CSRF-TOKEN': config.auth.csrfToken
        },
        data: {
            'name' :    userName,
            'email':    email,
            'address':  address,
            'photo':    image
        },
        success: updateDone,
        error: cantUpdate
    })
}

function updateDone(response)
{
    console.log(response);
    config.mainTemplate.prepend(messages.success.alert.replace(/@message/, response.message));
    $('.modal').modal('hide');
}

function cantUpdate(response)
{
    console.log(response);
    response = response.responseJSON;
    console.log(response.email)
    let errorMessage = "";
    if (response['email'] != null)
    {
        for (let i = 0; i < response.email.length; i++) {
            errorMessage += response.email[i] + '\n';
        }

        let emailMessage = messages.error.badge.replace(/@message/, errorMessage);
        $(emailMessage).insertAfter(emailField);
    }

    errorMessage = '';
    if (response.name != null)
    {
        for (let i = 0; i < response.name.length; i++) {
            errorMessage += response.name[i] + '\n';
        }

        let emailMessage = messages.error.badge.replace(/@message/, errorMessage);
        $(emailMessage).insertAfter(userNameField);
    }

    errorMessage = '';
    if (response.address != null)
    {
        for (let i = 0; i < response.address.length; i++) {
            errorMessage += response.address[i] + '\n';
        }

        let emailMessage = messages.error.badge.replace(/@message/, errorMessage);
        $(emailMessage).insertAfter(addressField);
    }

    errorMessage = '';
    if (response.photo != null)
    {
        for (let i = 0; i < response.photo.length; i++) {
            errorMessage += response.photo[i] + '\n';
        }

        let emailMessage = messages.error.badge.replace(/@message/, errorMessage);
        $(emailMessage).insertAfter(imageField);
    }

    errorMessage = '';
    if (response.password != null)
    {
        for (let i = 0; i < response.password.length; i++) {
            errorMessage += response.password[i] + '\n';
        }

        let emailMessage = messages.error.badge.replace(/@message/, errorMessage);
        $(emailMessage).insertAfter(imageField);
    }
}
