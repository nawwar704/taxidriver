require('../bootstrap')

$('#pointsCount').keyup(changeValue);

function changeValue(event) {
    let coinsIcon = '<i class="fa fa-coins"></i>';
    $('#pointsPrice').html($(this).val() + '$ ' + coinsIcon);
}
