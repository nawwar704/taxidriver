require('../bootstrap')

let homeUrl = '/home';
let user;

$(window).on('load', tryLoginWithLastToken)

function login()
{
    let email = $('#email').val();
    let password = $('#password').val();

    loginAjax(email, password, loginSuccess, loginError)

    function loginSuccess(response)
    {
        let accessToken = response['access_token'];
        let id = response.user['id'];
        let name = response.user['user_name'];

        localStorage.setItem('name', name);
        localStorage.setItem('email', email);
        localStorage.setItem('id', id);
        localStorage.setItem('accessToken', accessToken);


        redirectToHome(accessToken);
    }

    function loginError(response) {
        console.log(response)
    }
}

function redirectToHome(accessToken)
{
    let url = homeUrl + '?token=' + accessToken;
    window.open(url, '_self')
}

function loginAjax(email, password, loginSuccess, loginError)
{
    $.ajax({
        url: '/api/auth/login',
        type: 'POST',
        data: {
            'email': email,
            'password': password
        },
        success: loginSuccess,
        error: loginError
    });
}

function tryLoginWithLastToken() {
    let accessToken = localStorage.getItem('accessToken');

    //validateToken(accessToken);
}

function validateToken(accessToken) {
    $.ajax({
        url: '',
        data: {
            'token' : accessToken
        },
    })
}

//$('#loginButton').click(login)
