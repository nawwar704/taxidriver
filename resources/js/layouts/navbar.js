$('body').ready(function () {
    let body = $('body');
    let navbar = $('.navbar')
    let $window = $(window);

    body.css('paddingTop', navbar.outerHeight() + 'px')

    $window.resize(function () {
        body.css('paddingTop', navbar.outerHeight() + 'px')
    })
})
