<div class="modal fade" id="addressModal" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary-color text-secondary-color">
                <h5 class="modal-title" id="modalLabel">Address</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary-color text-primary-color">
                <form>
                    <div class="row">
                        <div class="form-group col-12">
                            <i class="fa fa-user-tie input-icon"></i>
                            <input id="address" type="text" class="form-control
                                            @error('address') is-invalid @enderror"
                                   name="address" value="{{ old('address') }}"
                                   placeholder="{{ __('Address') }}">
                        </div>
                        <div class="form-group col-12">
                            <button type="submit" class="btn btn-sm bg-primary-color">Edite
                                <i class="fa fa-edit text-secondary-color"></i>
                            </button>
                            <button class="btn btn-light btn-sm" data-dismiss="modal">Cancel
                                <i class="fa fa-ban text-secondary-color"></i>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
