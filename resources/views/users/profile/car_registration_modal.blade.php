<div class="modal fade" id="carModal" tabindex="-1" aria-labelledby="carModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary-color text-secondary-color">
                <h5 class="modal-title" id="carModalLabel">@yield('modal_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary-color text-primary-color">
                @include('driver.cars.car_registration_form')
            </div>
        </div>
    </div>
</div>
