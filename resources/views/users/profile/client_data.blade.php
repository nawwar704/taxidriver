<div class="card shadow-box">

    <div class="card-header bg-primary-color text-secondary-color">
        <h3>Client Data</h3>
    </div>

    <div class="card-body bg-secondary-color text-primary-color position-relative">
        <div class="row justify-content-around">

            <div class="col-sm-12 col-md-6 col-lg-3 mb-sm-2">
                <div class="col-">
                    <i class="fas fa-user-tie fa-2x fa-fw mr-1"></i>
                    <h4 class="d-inline">Name</h4>
                </div>
                <span class="client-data text-center">{{$user->name}}</span>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-3 mb-sm-2">
                <div class="col-">
                    <i class="fas fa-envelope fa-2x fa-fw mr-1"></i>
                    <h4 class="d-inline">Email</h4>
                </div>
                <span class="client-data text-center">{{$user->email}}</span>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-3 mb-sm-2">
                <div class="col-">
                    <i class="fas fa-address-card fa-2x fa-fw mr-1"></i>
                    <h4 class="d-inline">Address</h4>
                </div>

                <span class="client-data text-center">{{$user->address}}</span>
            </div>

            <div class="col-sm-12 col-md-6 col-lg-3">
                <div class="col-">
                    <i class="fas fa-balance-scale fa-2x fa-fw mr-1"></i>
                    <h4 class="d-inline justify-content-center">Points</h4>
                    <a class="btn btn-sm bg-primary-color text-secondary-color mb-2 ml-2" data-toggle="modal" data-target="#pointsModal">
                        <i class="fa fa-plus-circle"></i>
                    </a>

                </div>
                <span class="client-data text-center">{{$user->points}}</span>
            </div>
        </div>

        <div class="height-100px"></div>
        <img class="img-thumbnail img-fluid profile-image image" src="{{ asset('storage/personalphotos/' .
                auth()->user()->name . '/' . auth()->user()->name . '.png') }}">

        <div class="edit-delete-client-btn d-flex justify-content-end">
            <a class="btn text-primary-color" data-toggle="modal" data-target="#userEditModal">
                <i class="fa fa-edit"></i>
            </a>
            <a class="btn text-primary-color">
                <i class="fa fa-trash"></i>
            </a>
        </div>
    </div>
</div>
