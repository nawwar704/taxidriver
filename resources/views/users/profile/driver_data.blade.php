<div class="card shadow-box">
    <div class="card-header bg-primary-color text-secondary-color">
        <h3>Driver Data</h3>
    </div>
    <div class="card-body bg-secondary-color text-primary-color">
        <div class="row justify-content-around">

            <div class="col-12 col-md-3">
                <h4 class="mr-auto">License</h4>
                <img class="img-thumbnail img-fluid image" src="{{asset('images/profilePhotos/default.png')}}" alt="License Photo">
                <div class="mt-4 float-right float-md-left">
                    <a class="btn btn-sm bg-primary-color text-secondary-color"
                       data-toggle="modal" data-target="#modal">
                        Add driver license
                        <i class="fa fa-plus-circle"></i>
                    </a>
                </div>
            </div>

            <div class="col-12 col-md-7 car-section pl-md-5 mt-4 mt-md-0 d-flex">
                <div class="row pb-2 pl-3">
                    <h4 class="mb-0">Cars</h4>
                    <div class="float-right float-md-left">
                        <a class=" ml-2 btn btn-sm bg-primary-color" data-toggle="modal" data-target="#carModal">
                            Add a car
                            <i class="fa fa-plus-circle"></i>
                        </a>
                    </div>
                </div>

                @include('driver.cars.cars_accordion')
            </div>
        </div>

    </div>
</div>
