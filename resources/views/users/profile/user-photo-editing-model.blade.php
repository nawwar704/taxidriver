<div class="modal fade" id="userPhotoModal" tabindex="-1" aria-labelledby="carModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary-color text-secondary-color">
                <h5 class="modal-title" id="carModalLabel">@yield('modal_title')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary-color text-primary-color">
                <div class="container">
                    <div class="row justify-content-center">
                        <div class="col-md-8">

                            <form method="POST" action="{{ route('register') }}"> <!--Use driver register route-->
                                @csrf
                                <div class="form-group row w-100">
                                    <div class="col-">
                                        <div class="custom-file">
                                            <input id="photo" type="file" class="custom-file-input imageFileLoader
                                            @error('photo') is-invalid @enderror"
                                                   name="photo" value="{{ old('photo') }}">
                                            <label class="custom-file-label" for="photo">
                                                Upload your photo
                                            </label>
                                        </div>
                                        @error('photo')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                        @enderror
                                    </div>

                                    <div class="col- mt-4 text-center w-100">
                                        <img class="rounded img-thumbnail img-fluid mx-auto d-block imagePlace"
                                             src="{{asset('images/profilePhotos/default.png')}}">
                                    </div>
                                </div>
                                <div class="row justify-content-start">
                                    <div class="col-12">
                                        <div class="form-group ml-2">
                                            <button type="submit" class="btn btn-sm bg-primary-color">
                                                <i class="fa fa-plus-circle"></i>
                                                Add
                                            </button>
                                            <button class="btn btn-light btn-sm" data-dismiss="modal">
                                                <i class="da fa-ban"></i>
                                                Cancel
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
</div>
