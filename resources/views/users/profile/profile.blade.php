@extends('layouts.app')
@section('content')
<div class="container py-4">
    <div class="main-data">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item" role="presentation">
                <a class="nav-link bg-secondary-color text-primary-color active" id="pills-home-tab" data-toggle="pill" href="#pills-home" role="tab"
                   aria-controls="pills-home" aria-selected="true">Client Data</a>
            </li>
            <li class="nav-item" role="presentation">
                <a class="nav-link bg-secondary-color text-primary-color" id="pills-profile-tab" data-toggle="pill" href="#pills-profile" role="tab"
                   aria-controls="pills-profile" aria-selected="false">Driver Data</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                 aria-labelledby="pills-home-tab">
                @include('users.profile.client_data')
            </div>
            <div class="tab-pane fade" id="pills-profile" role="tabpanel" aria-labelledby="pills-profile-tab">
                @include('users.profile.driver_data')
            </div>
        </div>
    </div>
</div>
@include('users.profile.driver_license_registration_modal')
@include('users.profile.car_registration_modal') @include('users.points.add_points_form_modal')
@include('users.profile.driver_license_registration_modal') @include('users.profile.car_registration_modal')
@include('users.points.add_points_form_modal')
@include('users.profile.user-edit-modal')
@include('users.profile.car-editing-form-modal')
<script>
var msg = '{{Session::get('alert')}}';
var exist = '{{Session::has('alert')}}';
if(exist){
alert(msg);
}
</script>
@endsection
