<div class="modal fade" id="userEditModal" tabindex="-1" aria-labelledby="modalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary-color text-secondary-color">
                <h5 class="modal-title" id="modalLabel">Your name</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary-color text-primary-color">
                <form action="{{route('clients.update',auth()->user()) }}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="row">
                        <!-- Name field-->
                        <div class="form-group col-12">
                            <i class="fa fa-user-tie input-icon"></i>
                            <input id="userName" type="text" class="form-control
                                            @error('userName') is-invalid @enderror"
                                   name="name" value="{{$user->name}}"
                                   placeholder="{{ __('Your Name') }}">
                        </div>

                        <!-- Email field-->
                        <div class="form-group col-12">
                            <i class="fa fa-envelope input-icon"></i>
                            <input id="email" type="email" class="form-control
                                            @error('email') is-invalid @enderror"
                                   name="email" value="{{ $user->email }}"
                                   placeholder="{{ __('Your Email') }}">
                        </div>
                        <!-- Address field-->
                        <div class="form-group col-12">
                            <i class="fa fa-address-card input-icon"></i>
                            <input id="address" type="text" class="form-control
                                            @error('address') is-invalid @enderror"
                                   name="address" value="{{ $user->address }}"
                                   placeholder="{{ __('Address') }}">
                        </div>
                        <!-- Photo field-->
                        <div class="col-12">
                            <i class="fa fa-upload input-icon"></i>
                            <div class="custom-file">
                                <input id="photo" type="file" class="custom-file-input imageFileLoader
                                            @error('photo') is-invalid @enderror"
                                       name="photo" value="{{ $user->photo }}">
                                <label class="custom-file-label" for="photo">
                                    Upload your photo
                                </label>
                            </div>
                            @error('photo')
                            <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                            @enderror
                        </div>

                        <div class="col- mt-4 mb-4 text-center w-100">
                            <img class="rounded img-thumbnail img-fluid mx-auto d-block imagePlace image"
                                 src="{{asset('images/profilePhotos/default.png')}}">
                        </div>

                        <div class="form-group col-12">
                            <button id="editClientData" type="button" class="btn btn-sm bg-primary-color">Edit
                                <i class="fa fa-edit text-secondary-color"></i>
                            </button>
                            <button class="btn btn-light btn-sm" data-dismiss="modal">Cancel
                                <i class="fa fa-ban text-secondary-color"></i>
                            </button>

                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
