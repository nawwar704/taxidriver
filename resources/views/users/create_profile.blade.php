<h1> create profile</h1>
<form action="{{route('clients.store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="form-group">
        <label for="first_name">first name</label>
        <input type="text" class="form-control" id="first_name" name="first_name" placeholder="Enter your first name">

    </div>
    <div class="form-group">
        <label for="last_name">last name</label>
        <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Enter your last name">

    </div>


    <div class="form-group">
        <label for="address"> address</label>
        <input type="text" class="form-control" id="address" name='address' placeholder="Enter your address">

    </div>
    <div class="form-group">
        <label for="personal_photo"> personal photo</label>
        <input type="file" name='personal_photo' class="form-control" id="personal_photo"
            placeholder="please insert your photo">

    </div>
    <div class="form-group">
        <label for="points"> points</label>
        <input type="number" name="points" class="form-control" id="points" placeholder="please insert your points">


    </div>

    <button type="submit" class="btn btn-primary">Submit</button>


</form>
