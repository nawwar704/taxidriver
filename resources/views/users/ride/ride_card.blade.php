<div class="card text-center mt-4 shadow-box drop-down-animation">
    <div class="card-header bg-primary-color">
        Departure <i class="fa fa-arrow-circle-right text-secondary-color"></i> Destination
    </div>
    <div class="card-body bg-secondary-color text-primary-color">
        <h5 class="card-title">Car Driver</h5>
        <ul class="card-group justify-content-center pl-0 bg-transparent text-primary-color">
            <li class="list-group-item border-0 bg-transparent text-white">Name car driver name
                <i class="fa fa-user ml-2 text-primary-color"></i>
            </li>

            <li class="list-group-item border-0 bg-transparent text-white">Car Model car model
                <i class="fa fa-car ml-2 text-primary-color"></i>
            </li>

            <li class="list-group-item border-0 bg-transparent text-white">Car panel number 000000
                <i class="fa fa-list ml-2 text-primary-color"></i>
            </li>

            <li class="list-group-item border-0 bg-transparent text-white">Rating 5.0
                <i class="fa fa-star ml-2 text-primary-color text-primary-color"></i>
            </li>
        </ul>
        <a href="#" class="btn bg-primary-color text-secondary-color">Take the ride
            <i class="fa fa-vote-yea text-secondary-color"></i>
        </a>
    </div>
    <div class="card-footer text-muted bg-primary-color text-secondary-color">
        Pass expected time
    </div>
</div>
