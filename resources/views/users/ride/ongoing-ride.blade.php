<div class="fixed-bottom w-100 bg-primary-color text-secondary-color rounded-top ongoing-ride">
    <button type="button" class="close ongoing-ride-close">
        <span aria-hidden="true">&times;</span>
    </button>
    <div class="row p-5">
        <div class="col-12 col-md-9 col-lg-10">
            <div class="row">
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="row justify-content-center">
                        <div class="col-4 d-flex justify-content-center">
                            <i class="fa fa-user-tie fa-2x"></i>
                        </div>
                        <div class="col-8">
                            <div class="col-12">
                                Car Driver
                            </div>
                            <div class="col-12">
                                Name
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="row justify-content-center">
                        <div class="col-4 d-flex justify-content-center">
                            <i class="fa fa-car fa-2x"></i>
                        </div>
                        <div class="col-8">
                            <div class="col-12">
                                Car Model
                            </div>
                            <div class="col-12">
                                Brand / Model
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="row justify-content-center">
                        <div class="col-4 d-flex justify-content-center">
                            <i class="fa fa-clock fa-2x"></i>
                        </div>
                        <div class="col-8">
                            <div class="col-12">
                                Expected Time
                            </div>
                            <div class="col-12">
                                5:30 PM
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-12 col-md-5 col-lg-3">
                    <div class="row justify-content-center">
                        <div class="col-4 d-flex justify-content-center">
                            <i class="fa fa-location-arrow fa-2x"></i>
                        </div>
                        <div class="col-8">
                            <div class="col-12">
                                Departure As-Swaida
                            </div>
                            <div class="col-12">
                                Destination Damascus
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-3 col-lg-2 d-flex justify-content-center align-items-center mt-4 mt-md-0">
            <div class="spinner-grow" style="width: 3rem; height: 3rem;" role="status">
                <span class="sr-only">Loading...</span>
            </div>
        </div>
    </div>
</div>
