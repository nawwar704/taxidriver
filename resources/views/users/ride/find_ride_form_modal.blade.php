@extends('layouts.modal.modal_with_form')
@section('modal_title')
    Find a Ride<i class="fa fa-search text-secondary-color ml-2"></i>
@endsection
@section('modal_body')
@include('users.ride.find_ride_form')
@endsection
