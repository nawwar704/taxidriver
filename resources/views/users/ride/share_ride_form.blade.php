<div class="container">
    <div class="row justify-content-center">

        <form method="POST" action="{{ route('ride.share',auth()->user()) }}" class="col-12">
            <!--Use find a drive route-->
            @csrf
            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <input id="departureLocation" type="text" class="form-control
                                            @error('departureLocation') is-invalid @enderror"
                               name="departureLocation" value="{{ old('departureLocation') }}"
                               placeholder="{{ __('Departure Location') }}">
                        <i class="fa fa-map-marker text-primary-color input-icon"></i>
                    </div>

                    @error('departureLocation')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group w-100">
                <div class="row justify-content-center">

                    <div class="col-md-12">
                        <input id="time" type="time"
                               class="form-control @error('time') is-invalid @enderror"
                               name="time" value="{{ old('time') }}" placeholder="{{ __('Time') }}">
                        <i class="fa fa-clock text-primary-color input-icon"></i>
                    </div>

                    @error('time')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <input id="destination" type="text"
                               class="form-control @error('destination') is-invalid @enderror"
                               name="destination" value="{{ old('destination') }}" placeholder="{{ __('Destination Location') }}">
                        <i class="fa fa-map-marker text-primary-color input-icon"></i>
                    </div>

                    @error('destination')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <input id="car" type="number"
                               class="form-control @error('destination') is-invalid @enderror"
                               name="car" value="{{ old('car') }}" placeholder="{{ __('Car') }}">
                        <i class="fa fa-car text-primary-color input-icon"></i>
                    </div>

                    @error('destination')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <input id="seats" type="number"
                               class="form-control @error('seats') is-invalid @enderror"
                               name="seats" value="{{ old('seats') }}"
                               placeholder="{{ __('Number of Seats') }}">
                        <i class="fa fa-chair input-icon"></i>
                    </div>

                    @error('seats')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="form-group col-md-6 offset-md-4">
                    <button type="submit" class="btn btn-sm bg-primary-color">Share a Ride
                        <i class="fa fa-share text-secondary-color ml-2"></i>
                    </button>
                    <button class="btn btn-light btn-sm" data-dismiss="modal">Cancel
                        <i class="fa fa-ban text-secondary-color ml-2"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
