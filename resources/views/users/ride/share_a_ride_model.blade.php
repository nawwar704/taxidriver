<div class="modal fade" id="shareRideModal" tabindex="-1" aria-labelledby="shareRideModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-primary-color text-secondary-color">
                <h5 class="modal-title" id="shareRideModalLabel">Share a Ride<i class="fas fa-share ml-2"></i></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body bg-secondary-color text-primary-color">
                @include('users.ride.share_ride_form')
            </div>
        </div>
    </div>
</div>
