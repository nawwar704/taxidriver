@foreach ($clients as $user)
<a href="{{route('clients.show',$user)}}">{{$user->first_name}}</a>
<form action="{{route('clients.delete',$user)}}" method="POST">
    @csrf
    @method('DELETE')
    <div class="form-group">
        <input type="submit" class="btn btn-danger" value="Delete user">
    </div>
</form>
<a href="{{route('clients.edit',$user)}}">edit</a>

<img src="{{asset($user->personal_photo)}}" alt="profile Pic" height="200" width="200">
<h1>last_name{{$user->last_name}}</h1>
@endforeach
