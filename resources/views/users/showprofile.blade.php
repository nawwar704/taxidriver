@extends('layouts.app')
@section('content')
<div class="card">
    <div class="card-header">
        <h2 class="text-white">Profile</h2>
    </div>
    <div class="card-body">
        <div class="main-data">
            <ul class="list-group">
                <li class="list-group-item">Name User Name</li>
                <li class="list-group-item">Email User Email</li>
                <li class="list-group-item">Address User Address</li>
                <li class="list-group-item">Points User Points</li>
                <li class="list-group-item">
                    <img class="img-thumbnail img-fluid" src="{{asset('images/profilePhotos/defaultPhotos.png')}}">
                </li>
            </ul>
        </div>
    </div>
</div>
@endsection
