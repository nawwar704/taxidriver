<div class="fixed-jumbotron-height mb-5 ">
    <div class="jumbotron bg-secondary-color text-primary-color expand-width px-0 pb-4">
        <div class="static-width px-2">
            <h1 class="display-4">Share a Drive,  Get to your destination quickly</h1>
            <p class="lead">This is a simple hero unit, a simple jumbotron-style component for calling extra attention to featured content or information.</p>

            <div class="jumbotron-divider">
                <hr class="my-4">
                <p>It uses utility classes for typography and spacing to space content out within the larger container.</p>

                <a class="btn bg-primary-color btn-lg" data-toggle="modal" data-target="#modal" role="button">Find a Ride
                    <i class="fas fa-search text-secondary-color"></i>
                </a>
                <a class="btn bg-primary-color btn-lg" data-toggle="modal" data-target="#shareRideModal" role="button">Share a  Ride
                    <i class="fas fa-share text-secondary-color"></i>
                </a>
            </div>
        </div>
    </div>

</div>
