<div class="container">
    <div class="row justify-content-center">

        <form method="POST" action="{{ route('clients.add_points',$user) }}" class="col-12">
            <!--Use points adder route-->
            @csrf
            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <i class="fa fa-coins input-icon"></i>
                        <input id="pointsCount" type="number" class="form-control
                                            @error('pointsCount') is-invalid @enderror" name="pointsCount"
                            value="{{ old('pointsCount') }}">
                    </div>

                    @error('pointsCount')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>
            </div>

            <div class="row justify-content-center">
                <div class="bg-primary-color rounded-pill p-3 point-price-background">
                    <div id="pointsPrice" class="text-center text-secondary-color">
                        0$ <i class="fa fa-coins"></i>
                    </div>
                </div>
            </div>

            <div class="row justify-content-end">
                <div class="form-group">
                    <button type="submit" class="btn btn-sm bg-primary-color">
                        Add
                        <i class="fa fa-plus-circle"></i>
                    </button>
                    <button class="btn btn-light btn-sm" data-dismiss="modal">Cancel
                        <i class="fa fa-ban"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
