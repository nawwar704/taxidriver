@extends('layouts.app')

@section('content')
<div class="container login-card">
    <div class="row justify-content-center">
        <div class="col-md-5 col-12">
            <div class="card shadow-box">
                <div class="card-header bg-primary-color text-secondary text-center">{{ __('Login') }}</div>

                <div class="card-body bg-secondary-color text-primary-color">

                    <div class="login-icon d-flex justify-content-center mb-2 text-primary-color">
                        <i class="fa fa-user-circle fa-6x"></i>
                    </div>

                    <form method="POST" action="{{route('login')}}">
                        @csrf

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email" autofocus
                                    placeholder="{{ __('E-Mail Address') }}">
                                <i class="fa fa-envelope input-icon"></i>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control
                                       @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="current-password" placeholder="{{ __('Password') }}">
                                <i class="fa fa-key input-icon"></i>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="custom-control custom-switch">
                                    <input class="custom-control-input" type="checkbox" name="remember" id="remember"
                                        {{ old('remember') ? 'checked' : '' }}>

                                    <label class="custom-control-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0 px-3 justify-content-between">
                            <button id="loginButton" type="submit" class="btn bg-primary-color text-secondary-color">
                                {{ __('Login') }}
                            </button>

                            @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                            @endif
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
