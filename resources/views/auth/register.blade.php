@extends('layouts.app')

@section('content')
<div class="container mt-4">
    <div class="row justify-content-center">
        <div class="col-md-5">
            <div class="card shadow-box">
                <div class="card-header bg-primary-color text-secondary-color text-center">{{ __('Register') }}</div>

                <div class="card-body bg-secondary-color text-primary-color">
                    <form method="POST" action="{{ route('register')  }}" enctype="multipart/form-data">
                        @csrf

                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                    name="name" value="{{ old('name') }}" required autocomplete="name" autofocus
                                    placeholder="{{ __('Name') }}">
                                <i class="fa fa-user-tie input-icon"></i>
                                @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                    name="email" value="{{ old('email') }}" required autocomplete="email"
                                    placeholder="{{ __('E-Mail Address') }}">
                                <i class="fa fa-envelope input-icon"></i>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="phone" type="text" class="form-control @error('phone') is-invalid @enderror"
                                       name="phone" value="{{ old('phone') }}" required autocomplete="phone" autofocus
                                       placeholder="{{ __('Phone Number') }}">
                                <i class="fa fa-user-tie input-icon"></i>
                                @error('phone')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>


                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="address" type="text"
                                    class="form-control @error('address') is-invalid @enderror" name="address"
                                    value="{{ old('address') }}" required placeholder="{{ __('Address') }}">
                                <i class="fa fa-address-card input-icon"></i>
                                @error('address')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">

                            <div class="col-md-12">
                                <input id="password" type="password" class="form-control
                                        @error('password') is-invalid @enderror" name="password" required
                                    autocomplete="new-password" placeholder="{{ __('Password') }}">
                                <i class="fa fa-key input-icon"></i>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>




                        <div class="form-group row">
                            <div class="col-md-12">
                                <input id="password-confirm" type="password" class="form-control"
                                    name="password_confirmation" required autocomplete="new-password"
                                    placeholder="{{ __('Confirm Password') }}">
                                <i class="fa fa-check input-icon"></i>
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-12">
                                <div class="custom-file">
                                    <input id="photo" type="file" class="custom-file-input imageFileLoader
                                            @error('photo') is-invalid @enderror" name="photo"
                                        value="{{ old('photo') }}">
                                    <label class="custom-file-label" for="photo">
                                        <i class="fa fa-upload pr-2"></i>Upload a personal photo
                                    </label>
                                </div>
                                @error('photo')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>

                            <div class="col-md-6 mt-3">
                                <img class="rounded img-thumbnail img-fluid imagePlace"
                                    src="{{asset('images/profilePhotos/default.png')}}">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn bg-primary-color text-secondary-color">
                                    {{ __('Register') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
