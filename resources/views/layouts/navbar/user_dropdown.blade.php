<li class="nav-item dropdown">
    <a id="navbarDropdown" class="nav-link dropdown-toggle text-secondary-color" href="#" role="button"
       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
        {{ auth()->user()->name }} <span class="fa fa-user-circle"></span>
    </a>

    <div class="dropdown-menu dropdown-menu-right bg-primary-color" aria-labelledby="navbarDropdown">

        <a class="dropdown-item" id="profileButton" href="{{route('clients.profile', auth()->user()->getAuthIdentifier())}}">  <!--Route to profile page-->
            {{__('profile')}}
        </a>

        <div class="dropdown-divider">


        </div>


        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
            {{ __('Logout') }}
        </a>

        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
            @csrf
        </form>
    </div>
</li>
