<nav class="navbar navbar-expand-md navbar-light bg-primary-color shadow-sm fixed-top">
    <div class="container">
        <a class="navbar-brand go-right-then-hide-animation" href="{{ url('/') }}">
            <i class="fa fa-car-side text-secondary-color"></i>
            {{ config('app.name', 'Laravel') }}
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <!-- Left Side Of navbar -->
            <ul class="navbar-nav mr-auto">

            </ul>

            <!-- Right Side Of navbar -->
            <ul class="navbar-nav ml-auto">
                <!-- Authentication Links -->
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>

                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                @include('layouts.navbar.user_dropdown')
                @endguest
            </ul>
        </div>
    </div>
</nav>
