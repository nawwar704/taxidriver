@extends('layouts.app')
@section('content')
@include('users.main_page.jumbotron')
<div class="sidebar justify-content-center align-content-center">
    <div class="ride-title col-12">
        Ride
    </div>
    <div class="riders-section col-12">
        Riders
        <div class="riders-list">
            <ul>
                <li>Motasem Ghozlan</li>
                <li>Motasem Ghozlan</li>
                <li>Motasem Ghozlan</li>
                <li>Motasem Ghozlan</li>
                <li>Motasem Ghozlan</li>
                <li>Motasem Ghozlan</li>
            </ul>
        </div>
    </div>
    <div class="sidebar-toggle text-center align-content-center">&lt;</div>
</div>
<div class="container">
    <div class="row justify-content-center mb-5 pb-5">
        <div id="rides" class="col-12 p-4">
            @foreach($rides as $ride)
                <div class="card text-center mt-4 shadow-box drop-down-animation">
                    <div class="card-header bg-primary-color">
                        {{$ride['departure_location']}} <i class="fa fa-arrow-circle-right text-secondary-color"></i> {{$ride['destination']}}
                    </div>
                    <div class="card-body bg-secondary-color text-primary-color">
                        <h5 class="card-title">Car Driver</h5>
                        <ul class="card-group justify-content-center pl-0 bg-transparent text-primary-color">
                            <li class="list-group-item border-0 bg-transparent text-white">Name {{$ride->driver->user['name']}}
                                <i class="fa fa-user ml-2 text-primary-color"></i>
                            </li>

                            <li class="list-group-item border-0 bg-transparent text-white">Car Model {{$ride->car['model']}}
                                <i class="fa fa-car ml-2 text-primary-color"></i>
                            </li>

                            <li class="list-group-item border-0 bg-transparent text-white">Car panel number
                                {{$ride->car['panel_number']}}
                                <i class="fa fa-list ml-2 text-primary-color"></i>
                            </li>

                            <li class="list-group-item border-0 bg-transparent text-white">Rating 5.0
                                <i class="fa fa-star ml-2 text-primary-color text-primary-color"></i>
                            </li>
                        </ul>
                    </div>
                    <button type="submit" class="btn bg-primary-color text-secondary-color">
                        take the ride
                        <i class="fas fa-check-circle"></i> </button>


                    <div class="card-footer text-muted bg-primary-color text-secondary-color">
                        Pass expected time
                    </div>
                </div>

            @endforeach
        </div>
    </div>
</div>
@include('users.ride.find_ride_form_modal')
@include('users.ride.share_a_ride_model')
@include('users.ride.find_ride_form_modal')
@include('users.ride.share_a_ride_model')
@include('users.ride.ongoing-ride')
@endsection
