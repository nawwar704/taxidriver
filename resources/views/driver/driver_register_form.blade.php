<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            <form method="POST" action="{{ route('drivers.store',auth()->user()) }}"  enctype="multipart/form-data"> <!--Use driver register route-->
                @csrf
                <div class="form-group row w-100">
                    <div class="col-">
                        <div class="custom-file">
                            <input id="photo" type="file" class="custom-file-input imageFileLoader
                                            @error('photo') is-invalid @enderror"
                                   name="photo" value="{{ old('photo') }}">
                            <label class="custom-file-label" for="photo">
                                Upload your license photo
                            </label>
                        </div>
                        @error('photo')
                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                        @enderror
                    </div>

                    <div class="col- mt-4 text-center w-100">
                        <img class="rounded img-thumbnail img-fluid mx-auto d-block imagePlace"
                             src="{{asset('images/profilePhotos/default.png')}}">
                    </div>
                </div>
                <div class="row justify-content-center">
                    <div class="col-12">
                        <div class="form-group ml-2">
                            <button type="submit" class="btn btn-sm bg-primary-color">
                                <i class="fa fa-plus-circle"></i>
                                Add
                            </button>
                            <button class="btn btn-light btn-sm" data-dismiss="modal">
                                <i class="da fa-ban"></i>
                                Cancel
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

