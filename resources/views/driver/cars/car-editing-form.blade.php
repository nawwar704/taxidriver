<div class="container">
    <div class="row justify-content-center">

        <form method="POST" action="{{route('car.update',auth()->user())}}" class="col-12"> <!--Use car register route-->
            @csrf
            @method('PUT')
            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <i class="fa fa-car input-icon"></i>
                        <input id="model" type="text" class="form-control
                                            @error('model') is-invalid @enderror"
                               name="model" value="{{ old('model') }}"
                               placeholder="{{ __('Car model') }}">
                    </div>

                    @error('model')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <i class="fa fa-border-all input-icon"></i>
                        <input id="panelNumber" type="number"
                               class="form-control @error('panelNumber') is-invalid @enderror"
                               name="panelNumber" value="{{ old('panelNumber') }}"
                               placeholder="{{ __('Panel Number') }}">
                    </div>

                    @error('panelNumber')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>

            <div class="form-group w-100">
                <div class="row justify-content-center">
                    <div class="col-md-12">
                        <i class="fa fa-paint-brush input-icon"></i>
                        <input id="color" type="text"
                               class="form-control @error('color') is-invalid @enderror"
                               name="color" value="{{ old('color') }}"
                               placeholder="{{ __('Color') }}">
                    </div>

                    @error('color')
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                    @enderror
                </div>
            </div>
            <div class="row justify-content-start">
                <div class="form-group col-12">
                    <button type="submit" class="btn btn-sm bg-primary-color">
                        Edit
                        <i class="fa fa-plus-circle"></i>
                    </button>
                    <button class="btn btn-light btn-sm" data-dismiss="modal">Cancel
                        <i class="fa fa-ban"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>
