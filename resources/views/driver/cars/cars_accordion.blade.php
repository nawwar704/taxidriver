<div class="accordion" id="accordionExample">
    @if(isset($cars))
        @foreach($cars as $car)
            <div class="card">

                <div class="card-header " id="car1">
                    <h2 class="mb-0">
                        <button class="btn btn-link btn-block text-left" type="button" data-toggle="collapse"
                                data-target="#carData1" aria-expanded="false" aria-controls="carData1">
                            <h4>{{$car->model}}</h4>
                        </button>
                    </h2>
                </div>


                <div id="carData1" class="collapse" aria-labelledby="car1" data-parent="#accordionExample">

                    <div class="card-body">
                        <div class="row justify-content-around">

                            <div class=""><h4>Model</h4>{{$car->model}}</div>
                            <div class=""><h4>Panel Number</h4> {{$car->panel_number}}</div>
                            <div class=""><h4>Color</h4> {{$car->color}}</div>
                        </div>
                    </div>
                    <div class="card-footer bg-secondary-color text-primary-color">
                        <a class="btn btn-sm bg-primary-color text-secondary-color" data-toggle="modal"
                           data-target="#carEditingModal">Edit <i class="fa fa-edit"></i></a>

                        <a class="btn btn-sm btn-danger text-secondary-color" type="submit">Delete <i
                                class="fa fa-trash"></i></a>

                    </div>

                </div>
            </div>
        @endforeach
    @endif
</div>
