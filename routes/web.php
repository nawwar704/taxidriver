<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|//
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect(route('home'));
});


Route::get('/clients/profile/{client}', "ClientController@show")->name('clients.profile');


Route::put('/clients/update/{client}', 'ClientController@update')->name('clients.update');
Route::get('/home', 'HomeController@index')->name('home');



Route::post('/car/register/{user}', 'CarController@store')->name('car.register');
Route::post('/riders/addpoints/{client}', 'ClientController@addpoints')->name('clients.add_points');
Route::post('/drivers/register/{user}', 'DriverController@store')->name('drivers.store');

Route::put('/car/update/{user}','CarController@store')->name('car.update');
Route::delete('/car/delete/{car}','CarController@destroy')->name('car.delete');
Route::post('/ride/find/{user}','RideController@findRide')->name('ride.find');
Route::post('/ride/share/{user}','RideController@shareRide')->name('ride.share');
Route::post('/ride/{user}/{ride}','RideController@bookRide')->name('ride.book');
Route::get('/home/index','HomeController@redirect')->name('notoken');

Auth::routes();
